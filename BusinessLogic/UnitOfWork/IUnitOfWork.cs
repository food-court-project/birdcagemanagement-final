using DataAccess.IRepository;
using DataAccess.Repository.IRepository;

namespace BusinessLogic.UnitOfWork;

public interface IUnitOfWork
{
    //khai bao properties cua Repository

    public ICustomerRepository CustomerRepository { get; }
    public IExpenseRepository ExpenseRepository { get; }
    public IInventoryRepository InventoryRepository { get; }
    public IMaterialRepository MaterialRepository { get; }
    public IOrderItemRepository OrderItemRepository { get; }
    public IOrderRepository OrderRepository { get; }
    public IProductRepository ProductRepository { get; }
    public IStaffRepository StaffRepository { get; }
    public IWorkOnRepository WorkOnRepository { get; }
    public Task<int> SaveChange();
}